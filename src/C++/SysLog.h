/* -*- C++ -*- */

/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifdef HAVE_SYSLOG_H
#ifndef FIX_SYSLOG_H
#define FIX_SYSLOG_H

#ifdef _MSC_VER
#pragma warning( disable : 4503 4355 4786 4290 )
#endif

#include "Log.h"
#include "SessionSettings.h"
#include <fstream>
#include <string>
#include <syslog.h>

namespace FIX
{
/// syslog based implementation of Log.
class SysLog : public Log
{
public:
  SysLog() : session_id(""), session_id_size(0)  { }
  SysLog( const SessionID& s ) :
    session_id(s.toStringFrozen()),
    session_id_size(s.toStringFrozen().size())
  { }

  ~SysLog();

  void clear();
  void backup();

  void setIdentity(const std::string& s);
  void setOption(int opt);
  void setFacility(int fac);
  void setPriority(int pri);

  void onIncoming( const std::string& value )
  { syslog_(value); }
  void onOutgoing( const std::string& value )
  { syslog_(value); }
  void onEvent( const std::string& value )
  { syslog_(value); }

private:
  void inline syslog_(const std::string& value)
  {
    if (session_id_size)
      syslog(syslog_priority, "%s %s", session_id.c_str(), value.c_str());
    else
      syslog(syslog_priority, "%s", value.c_str());
  }

  /* configuration information about the syslog set up
   */

  std::string session_id;
  std::string::size_type session_id_size;
  std::string syslog_ident;
  int syslog_option;
  int syslog_facility;
  int syslog_priority;
};

class SysLogFactory : public LogFactory
{
public:
  static const std::string DEFAULT_SYSLOG_IDENTITY;
  static const int DEFAULT_SYSLOG_OPTION;
  static const int DEFAULT_SYSLOG_FACILITY;
  static const int DEFAULT_SYSLOG_PRIORITY;
  SysLogFactory() :
    m_useSettings( false ),
    syslog_ident( DEFAULT_SYSLOG_IDENTITY ),
    syslog_option( DEFAULT_SYSLOG_OPTION ),
    syslog_facility( DEFAULT_SYSLOG_FACILITY ),
    syslog_priority( DEFAULT_SYSLOG_PRIORITY )
  {
      openlog_();
  }

  SysLogFactory(const std::string& name) :
    m_useSettings( false ),
    syslog_ident( name ),
    syslog_option( DEFAULT_SYSLOG_OPTION ),
    syslog_facility( DEFAULT_SYSLOG_FACILITY ),
    syslog_priority( DEFAULT_SYSLOG_PRIORITY )
  {
      openlog_();
  }

  /* ignores SessionSettings, but accepts it for API compatibility */
  SysLogFactory( const SessionSettings& settings ) :
    m_settings( settings ),
    m_useSettings( true ) 
  {
      /* FIXME: read the actual config */
      syslog_ident = DEFAULT_SYSLOG_IDENTITY;
      syslog_option = DEFAULT_SYSLOG_OPTION;
      syslog_facility = DEFAULT_SYSLOG_FACILITY;
      openlog_();
    try { 
        ;
    }
    catch( ConfigError& ) {}
  }

  /* ignores SessionSettings, but accepts it for API compatibility */
  SysLogFactory( const SessionSettings& settings, const std::string& name ) :
    m_settings( settings ),
    m_useSettings( true ),
    syslog_ident( name )
  {
      /* FIXME: read the actual config */
      syslog_option = DEFAULT_SYSLOG_OPTION;
      syslog_facility = DEFAULT_SYSLOG_FACILITY;
      openlog_();
    try { 
        ;
    }
    catch( ConfigError& ) {}
  }

  Log* create();
  Log* create( const SessionID& );
  void destroy( Log* );
private:
  void openlog_()
  {
      openlog(syslog_ident.c_str(), syslog_option, syslog_facility);
  }

  void initLog( SysLog* log );

  SessionSettings m_settings;
  bool m_useSettings;

  std::string syslog_ident;
  int syslog_option;
  int syslog_facility;
  int syslog_priority;
};
}

#endif //FIX_SYSLOG_H
#else // HAVE_SYSLOG
#error SysLog.h included, but HAVE_SYSLOG not defined
#endif
