/****************************************************************************
** Copyright (c) quickfixengine.org  All rights reserved.
**
** This file is part of the QuickFIX FIX Engine
**
** This file may be distributed under the terms of the quickfixengine.org
** license as defined by quickfixengine.org and appearing in the file
** LICENSE included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.quickfixengine.org/LICENSE for licensing information.
**
** Contact ask@quickfixengine.org if any conditions of this licensing are
** not clear to you.
**
****************************************************************************/

#ifdef _MSC_VER
#include "stdafx.h"
#else
#include "config.h"
#endif

#ifdef HAVE_SYSLOG_H

#include "SysLog.h"
#include "SessionID.h"
#include "SessionSettings.h"
#include "Utility.h"
#include "strptime.h"

namespace FIX
{
const std::string SysLogFactory::DEFAULT_SYSLOG_IDENTITY = "fix";
const int SysLogFactory::DEFAULT_SYSLOG_OPTION = LOG_NDELAY | LOG_NOWAIT;
const int SysLogFactory::DEFAULT_SYSLOG_FACILITY = LOG_USER;
const int SysLogFactory::DEFAULT_SYSLOG_PRIORITY = LOG_INFO;

void SysLog::setIdentity(const std::string& s)
{
  syslog_ident = s;
}

void SysLog::setOption(int opt)
{
  syslog_option = opt;
}
 
void SysLog::setFacility(int fac)
{
  syslog_facility = fac;
}

void SysLog::setPriority(int pri)
{
  syslog_priority = pri;
}

SysLog::~SysLog()
{
  closelog();
}

Log* SysLogFactory::create()
{
  SysLog *sLog = new SysLog();
  initLog(sLog);
  return sLog;
}

Log* SysLogFactory::create( const SessionID& s )
{
  SysLog *sLog = new SysLog(s);
  initLog( sLog );
  return sLog;
}

void SysLogFactory::initLog( SysLog *log )
{
  log->setIdentity( syslog_ident );
  log->setOption( syslog_option );
  log->setFacility( syslog_facility );
  log->setPriority( syslog_priority );
}

void SysLogFactory::destroy( Log* pLog )
{
  delete pLog;
}

void SysLog::clear()
{
}

void SysLog::backup()
{
}
} // namespace FIX

#endif //HAVE_SYSLOG_H 
